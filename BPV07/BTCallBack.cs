namespace BPV07
{
    using System.Linq;
    using System.Text.RegularExpressions;
    using Android.Content;
    using Android.Support.V4.Content;
    using Android.Bluetooth;
    using Config;
    using profile;
    using Java.Util;

    public class BTCallBack : BluetoothGattCallback
    {
        public GattStatus Status { get; set; }
        private BluetoothGatt Gatt { get; set; }
        
        public BluetoothGattService btService;
        public override void OnConnectionStateChange(BluetoothGatt gatt, GattStatus status, ProfileState newState)
        {
            base.OnConnectionStateChange(gatt, status, newState);
            Gatt = gatt;
            Status = status;
            if (status != GattStatus.Success)
            {
                return;
            }

            LocalBroadcastManager.GetInstance(MainActivity.AppContext).RegisterReceiver(new BCastR() { Gatt = Gatt }, getFilter());

            if (!gatt.DiscoverServices())
            {
                return;
            }

            if (newState != ProfileState.Connected)
            {
                Intent mIntent = new Intent(BleBroadCast.WANT_CONNECT_DEVICE);

                mIntent.PutExtra(BleIntentPut.BLE_DEVICE_ADDRESS, gatt.Device.Address);
                var pe = Regex.Unescape("\\u6211\\u60f3\\u8981\\u8fde\\u63a5\\u8bbe\\u5907");
                mIntent.PutExtra(BleIntentPut.BLE_OPTION, pe);
                mIntent.PutExtra(IntentPut.BLE_CONNECT_TYPE, 1);

                LocalBroadcastManager.GetInstance(MainActivity.AppContext).SendBroadcast(mIntent);
            }
        }

        public bool IsRunning;
        public override void OnCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic)
        {
            base.OnCharacteristicChanged(gatt, characteristic);

            UUID uuidString = characteristic.Uuid;
            if (uuidString.Equals(BleProfile.BATTERY_READ_UUID))
            {
                byte[] value = characteristic.GetValue();
                System.String action = BleProfileUtil.GetAction(uuidString, value);
                if (action == BleProfile.BP_OPRATE && !IsRunning)
                {
                    IsRunning = true;
                }
                else if (action == BleProfile.BP_OPRATE)
                {
                    IsRunning = false;
                    MainActivity.Current.ShowBP(value[1], value[2]);
                }
            }
        }

        public System.Action ServiceCreated;
        public override void OnServicesDiscovered(BluetoothGatt gatt, GattStatus status)
        {
            base.OnServicesDiscovered(gatt, status);
            
            foreach (BluetoothGattService bgs2 in gatt.Services)
            {                
                if (bgs2.Uuid.Equals(BleProfile.BP_SERVICE_UUID))
                {
                    bgs2.GetCharacteristic(BleProfile.BP_READ_UUID);
                    btService = new BPService(BleProfile.BP_SERVICE_UUID, GattServiceType.Secondary);

                    ServiceCreated?.Invoke();
                }
            }
            
            BluetoothGattCharacteristic TxChar = Gatt.GetService(BleProfile.BATTERY_SERVICE_UUID).GetCharacteristic(BleProfile.BATTERY_READ_UUID);

            TxChar.SetValue(BleIntentPut.BLE_CMD); 
            Gatt.SetCharacteristicNotification(TxChar, true);
            BluetoothGattDescriptor descriptor = TxChar.GetDescriptor(BleProfile.DEC_2);
            if (descriptor != null)
            {
                descriptor.SetValue(BluetoothGattDescriptor.EnableNotificationValue.ToArray());
                Gatt.WriteDescriptor(descriptor);
            }
        }

        private IntentFilter getFilter()
        {
            IntentFilter mIntentFilter = new IntentFilter();
            mIntentFilter.AddAction(BleBroadCast.WANT_CONNECT_DEVICE);
            mIntentFilter.AddAction(BleBroadCast.WANT_DISCONNECTING_DEVICE);
            mIntentFilter.AddAction(BleBroadCast.AUTO_CONNECT_DEVICE);
            mIntentFilter.AddAction(BleBroadCast.BATTERY_SERVER);
            mIntentFilter.AddAction(BleBroadCast.BP_SERVER);
            mIntentFilter.AddAction(BleBroadCast.BP_ADC);
            mIntentFilter.AddAction(BleProfile.BP_OPRATE);
            mIntentFilter.AddAction(BleBroadCast.CONNECTED_DEVICE_SUCCESS);

            return mIntentFilter;
        }
        
    }
}