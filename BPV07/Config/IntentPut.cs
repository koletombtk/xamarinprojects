namespace BPV07.Config
{
    public static class IntentPut
    {
        public static string BLE_CONNECT_TYPE = "connect_type";
        public static string PERSON_B0RN = "registerinfo_person_born";
        public static string PERSON_HEIGHT = "registerinfo_person_height";
        public static string PERSON_SEX = "registerinfo_person_sex";
        public static string PERSON_WEIGHT = "registerinfo_person_weight";
        public static int TYPE_AUTO_CONNECT = 0;
        public static int TYPE_HANDLE_CONNECT = 1;
    }
}