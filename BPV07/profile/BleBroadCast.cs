namespace BPV07.profile
{
    public static class BleBroadCast
    {
        public static string AUTO_CONNECT_DEVICE = "auto_connect_device";
        public static string BATTERY_SERVER = "bbc_batter_server";
        public static string BATTERY_SERVER_ORIGINAL = "bbc_battery_server_original";
        public static string BATTERY_UPDATE_LEVEL = "battery_update_level";
        public static string BLUETOOTH_OPEN_CLOSE = "android.bluetooth.adapter.action.STATE_CHANGED";
        public static string BP_ADC = "bp_adc";
        public static string BP_SERVER = "bp_server";
        public static string CONNECTED_DEVICE_SUCCESS = "bbc_connecting_device_success";
        public static string CONNECTING_DEVICE = "bbc_connecting_device";
        public static string CONNECTING_DEVICE_AND_FINDER_SERVER = "bbc_connecting_device";
        public static string DISCONNECTED_DEVICE_SUCCESS = "bbc_disconnected_device_success";
        public static string MUST_UPDATE_OAD = "must_update_oad";
        public static string ORIGINAL_DATE_UPDATE = "original_date_update";
        public static string PASSWORD_CHECK_FAIL = "bbc_check_pasword_fail";
        public static string PASSWORD_CHECK_SUCCESS = "bbc_check_pasword_success";
        public static string PASSWORD_MODIFY_FAIL = "bbc_password_modify_fail";
        public static string PASSWORD_MODIFY_SUCCESS = "bbc_password_modify_success";
        public static string READ_DEVICE_DATA_AFTER_BINDER = "read_device_data_after_binder";
        public static string READ_DEVICE_DATA_FINISH = "read_device_data_finish";
        public static string SERVER_PROBLEM = "bbc_server_problem";
        public static string WANT_CONNECT_DEVICE = "bbc_want_connect_device";
        public static string WANT_DISCONNECTING_DEVICE = "bbc_want_disconnecting_device";
        public static string WANT_PASSWORD_CHECK = "bbc_want_password_check"; 
    }
}