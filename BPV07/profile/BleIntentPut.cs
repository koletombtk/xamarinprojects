namespace BPV07.profile
{
    public static class BleIntentPut
    {
        public static string BLE_CMD = "ble_cmd";
        public static string BLE_DEVICE_ADDRESS = "ble_device_address";
        public static string BLE_OPTION = "ble_optinon";
        public static string BLE_PWD = "ble_pwd";
    }
}