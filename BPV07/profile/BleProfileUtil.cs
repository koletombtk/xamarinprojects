namespace BPV07.profile
{
    using System.Text.RegularExpressions;
    using Java.Util;
    public class BleProfileUtil:BleProfile
    {
        private static string TAG = typeof(BleProfileUtil).Name;

        public static string GetAction(UUID uuid, byte[] value)
        {
            if (uuid.Equals(BATTERY_READ_UUID) || uuid.Equals(BATTERY_CONFIG_UUID) || uuid.Equals(BP_READ_UUID))
            {
                return GetBatteryService(value[0]);
            }
            return "";
        }

        private static string GetBatteryService(byte byteValue)
        {
            var value = (sbyte) byteValue;
            if (value == -95)
            {
                return BleProfile.BATTERY_PASSWROD_CHECK;
            }
            if (value == -32)
            {
                return BleProfile.READ_BATTERY_SLEEP;
            }
            if (value == -47)
            {
                return BleProfile.READ_BATTERY_ORIGINAL;
            }
            if (value == -31)
            {
                return BleProfile.LONG_SERAT_OPRATE;
            }
            if (value == -85)
            {
                return BleProfile.ALARM_OPRATE;
            }
            if (value == -86)
            {
                return BleProfile.NIGHT_TURN_OPEATE;
            }
            if (value == -96)
            {
                return BleProfile.READ_BATTERY_OPRATE;
            }
            if (value == -111)
            {
                return BleProfile.BP_MODEL_OPRATE;
            }
            if (value == -63)
            {
                return BleProfile.PHONE_MESSAGE;
            }
            if (value == -89)
            {
                return BleProfile.DEVICE_FUNCTION;
            }
            if (value == -92)
            {
                return BleProfile.DEVICE_NUMBER;
            }
            if (value == -48)
            {
                return BleProfile.RATE_CURRENT_READ_OPRATE;
            }
            if (value == -88)
            {
                return BleProfile.READ_CURRENT_SPORT_OPRATE;
            }
            if (value == -112)
            {
                return BleProfile.BP_OPRATE;
            }
            if (value == -93)
            {
                return BleProfile.PERSON_INFO_OPRATE;
            }
            if (value == -41)
            {
                return BleProfile.DRINK_OPRATE;
            }
            if (value == -62)
            {
                return BleProfile.HEAD_SEND_CONTENT_TO_WATCH_OPRATE;
            }
            if (value == -83)
            {
                return BleProfile.DEVICE_MSG_OPRATE;
            }
            if (value == -82)
            {
                return BleProfile.FIND_WATCH_BY_PHON_OPRATE;
            }
            if (value == -81)
            {
                return BleProfile.DISCONNECT_WATCH_OPRATE;
            }
            if (value == -30)
            {
                return BleProfile.CHECK_WEAR_OPRATE;
            }
            if (value == -94)
            {
                return BleProfile.OAD_CMD_OPRATE;
            }
            if (value == -72)
            {
                return BleProfile.PERSON_WATCH_SETTING_OPRATE;
            }
            if (value == -74)
            {
                return BleProfile.TAKE_PHOTO_OPRATE;
            }
            if (value == -75)
            {
                return BleProfile.FIND_PHONE_BY_WATCH_OPERATE;
            }
            if (value == -12)
            {
                return BleProfile.CHANGE_WATCH_LANGUAGE_OPRATE;
            }
            return Regex.Unescape( "\\u6ca1\\u6709\\u6dfb\\u52a0Action");
        }
    }
}