namespace BPV07
{
    using System;
    using Android.Bluetooth;
    using Android.Runtime;
    using Java.Util;
    public class BPService : BluetoothGattService
    {
        public BPService(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
        }

        public BPService(UUID uuid, GattServiceType serviceType) : base(uuid, serviceType)
        {
        }

        public override BluetoothGattCharacteristic GetCharacteristic(UUID uuid)
        {
            var result = base.GetCharacteristic(uuid);
            return result;
        }
    }

}