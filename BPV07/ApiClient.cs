namespace BPV07
{
    using System;
    using System.Net;
    using System.Net.Http;
    using System.Text;
    using System.Threading.Tasks;
    using Extensions;
    using Org.Json;
    public static class ApiClient
    {
        static string storeKey = "logindata";
        static Uri _baseAddress = new Uri("http://koletom.hu");

        private static string _email;
        public static string Email
        {
            get { return _email; }
            set
            {
                _email = value;
                saveAuthData();
            }
        }

        private static string _password;
        public static string Password
        {
            get { return _password; }
            set
            {
                _password = value;
                saveAuthData();
            }
        }

        static void saveAuthData()
        {
            JSONObject jsonObject = new JSONObject();
            jsonObject.Put("email", Email ?? "");
            jsonObject.Put("password", Password ?? "");

            ApplicationExtension.SaveAppData(storeKey, jsonObject.ToString());
        }


        static ApiClient()
        {
            try
            {
                var loginData = ApplicationExtension.GetAppData(storeKey);
                if (!string.IsNullOrEmpty(loginData))
                {
                    JSONObject jsonObject = new JSONObject(loginData);
                    Email = jsonObject.GetString("email");
                    Password = jsonObject.GetString("password");
                }

            }
            catch { }

        }

        static HttpClient ClientFactory()
        {
            if (string.IsNullOrEmpty(Email) || string.IsNullOrEmpty(Password))
            {
                throw new UnauthorizedAccessException();
            }

            var client = new HttpClient() { BaseAddress = _baseAddress };
            client.DefaultRequestHeaders.Authorization = Email.ToAuthHeaderValue(Password);

            return client;
        }

        public static bool CheckConnection()
        {
            var result = Task.Run(() =>
            {
                try
                {
                    using (var client = ClientFactory())
                    {
                        var lgnTest = client.GetAsync("api/V07/authtest").Result;
                        if (lgnTest.StatusCode == HttpStatusCode.Unauthorized)
                        {
                            return false;
                        }

                        return true;
                    }
                }
                catch (UnauthorizedAccessException)
                {

                    return false;
                }

            }).Result;

            return result;
        }

        public static bool SendBP(byte sys, byte dia)
        {
            var result = Task.Run(() =>
            {
                try
                {
                    using (var client = ClientFactory())
                    {

                        JSONObject jsonObject = new JSONObject();

                        jsonObject.Put("Sys", sys);
                        jsonObject.Put("Dia", dia);

                        var reponse = client.PostAsync("/api/v07/addbp", new StringContent(jsonObject.ToString(), Encoding.UTF8, "application/json")).Result;

                        if (reponse.StatusCode == HttpStatusCode.Unauthorized)
                        {
                            return false;
                        }

                        return true;
                    }
                }
                catch (Exception)
                {

                    return false;
                }

            }).Result;
            return result;
        }
    }
}