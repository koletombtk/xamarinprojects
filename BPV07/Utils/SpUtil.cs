namespace BPV07.Utils
{
    using Android.Content;

    public class SpUtil
    {
        public static string SP_NAME;
        private static ISharedPreferences sp;

        static SpUtil(){
        SP_NAME = "hygieiatxt";
    }

    public static void clear(Context context)
    {
        if (sp == null)
        {
            sp = context.GetSharedPreferences(SP_NAME, 0);
        }
        sp.Edit().Clear().Commit();
    }

    public static void savestring(Context context, string key, string value)
    {
        if (sp == null)
        {
            sp = context.GetSharedPreferences(SP_NAME, 0);
        }
        sp.Edit().PutString(key, value).Commit();
    }

    public static void saveBoolean(Context context, string key, bool value)
    {
        if (sp == null)
        {
            sp = context.GetSharedPreferences(SP_NAME, 0);
        }
        sp.Edit().PutBoolean(key, value).Commit();
    }

    public static string getstring(Context context, string key, string defValue)
    {
        if (sp == null)
        {
            sp = context.GetSharedPreferences(SP_NAME, 0);
        }
        return sp.GetString(key, defValue);
    }

    public static bool getBoolean(Context context, string key, bool defValue)
    {
        if (sp == null)
        {
            sp = context.GetSharedPreferences(SP_NAME, 0);
        }
        return sp.GetBoolean(key, defValue);
    }

    public static int getInt(Context context, string key, int defValue)
    {
        if (sp == null)
        {
            sp = context.GetSharedPreferences(SP_NAME, 0);
        }
        return sp.GetInt(key, defValue);
    }

    public static long GetLong(Context context, string key, long defValue)
    {
        if (sp == null)
        {
            sp = context.GetSharedPreferences(SP_NAME, 0);
        }
        return sp.GetLong(key, defValue);
    }

    public static float getFloat(Context context, string key, float defValue)
    {
        if (sp == null)
        {
            sp = context.GetSharedPreferences(SP_NAME, 0);
        }
        return sp.GetFloat(key, defValue);
    }

    public static void cleanData(Context context, string key)
    {
        if (sp == null)
        {
            sp = context.GetSharedPreferences(SP_NAME, 0);
        }
        sp.Edit().PutString( key, "clean").Commit();
    }

    public static void saveInt(Context context, string key, int value)
    {
        if (sp == null)
        {
            sp = context.GetSharedPreferences(SP_NAME, 0);
        }
        sp.Edit().PutInt(key, value).Commit();
    }

    public static void saveLong(Context context, string key, long value)
    {
        if (sp == null)
        {
            sp = context.GetSharedPreferences(SP_NAME, 0);
        }
        sp.Edit().PutLong(key, value).Commit();
    }

    public static void saveFloat(Context context, string key, float value)
    {
        if (sp == null)
        {
            sp = context.GetSharedPreferences(SP_NAME, 0);
        }
        sp.Edit().PutFloat(key, value).Commit();
    }
}
}