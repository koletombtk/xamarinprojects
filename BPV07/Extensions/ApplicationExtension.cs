
namespace BPV07.Extensions
{
    using Android.App;
    using Android.Content;
    public static class ApplicationExtension
    {
        public static string GetAppData( string key)
        {
            var prefs = Application.Context.GetSharedPreferences(Application.Context.PackageName, FileCreationMode.Private);
            return prefs.GetString(key, string.Empty);
        }

        public static void SaveAppData(string key, string value)
        {
            var prefs = Application.Context.GetSharedPreferences(Application.Context.PackageName, FileCreationMode.Private);
            var prefEditor = prefs.Edit();
            prefEditor.PutString(key, value);
            prefEditor.Commit();
        }
    }
}