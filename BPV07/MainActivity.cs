﻿using Android.Views.InputMethods;

namespace BPV07
{
    using System;
    using Android.App;
    using Android.Bluetooth;
    using Android.Content;
    using Android.Content.PM;
    using Android.Views;
    using Android.Widget;
    using Android.OS;
    using profile;
    using Java.Util;
   

    [Activity(Label = "BPV07", MainLauncher = true, Icon = "@drawable/icon", ScreenOrientation = ScreenOrientation.Portrait)]
    public class MainActivity : Activity
    {
        private byte lastSys;
        private byte lastDia;

        private int prevContent = -1;

        BluetoothAdapter bluetoothAdapter = null;
        public BTCallBack btCallBack = new BTCallBack();
        private BluetoothGatt btGatt;
        public static System.Timers.Timer timer = new System.Timers.Timer();
        public static MainActivity Current;

        private BluetoothGattCharacteristic mBPMCharacteristic, mICPCharacteristic;
        public static Context AppContext;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            AppContext = Application.Context;
            Current = this;

            BluetoothManager btManager = (BluetoothManager)GetSystemService(Context.BluetoothService);
            var mBluetoothAdapter = btManager.Adapter;
            var pairedDevices = mBluetoothAdapter.BondedDevices;

            var bs = BluetoothService;
            foreach (var bluetoothDevice in pairedDevices)
            {
                if (bluetoothDevice.Name.Equals("V07", StringComparison.CurrentCultureIgnoreCase))
                {
                    btGatt = bluetoothDevice.ConnectGatt(this, true, btCallBack);
                }
            }

            if (btGatt == null)
            {
                Process.KillProcess(Process.MyPid());
            }            

            timer.Interval = 1000;
            timer.Elapsed += (s, e) =>
            {
                FindViewById<ProgressBar>(Resource.Id.progress).Progress++;
                if (FindViewById<ProgressBar>(Resource.Id.progress).Progress > 60)
                {
                    timer.Stop();
                    ShowContent(Resource.Layout.Main);
                    ShowToast("ERROR");
                }
            };

            btCallBack.ServiceCreated += () => RunOnUiThread(() => ShowContent(Resource.Layout.Main));

            btGatt.DiscoverServices();            
        }

        private void StartProgress(object sender, EventArgs e)
        {
            if (btCallBack.Status == GattStatus.Success)
            {
                _endProgress = false;

                if (btCallBack.btService != null)
                {
                    Window.AddFlags(WindowManagerFlags.KeepScreenOn);
                    btCallBack.IsRunning = false;

                    ShowContent(Resource.Layout.BpView);

                    var srv = btGatt.GetService(BleProfile.BATTERY_SERVICE_UUID);
                    BluetoothGattCharacteristic writeChar = srv.GetCharacteristic(BleProfile.BATTERY_CONFIG_UUID);

                    btGatt.SetCharacteristicNotification(writeChar, true);

                    writeChar.SetValue(BleProfile.READ_BP_NORMAL);

                    btGatt.SetCharacteristicNotification(writeChar, true);

                    writeChar.WriteType = GattWriteType.Default;
                    btGatt.WriteCharacteristic(writeChar);
                }
            }
            else
            {
                _endProgress = true;
            }
        }

        public BluetoothGattService GetService(UUID serviceId)
        {
            foreach (var gts in btGatt.Services)
            {
                if (gts.Uuid.Equals(serviceId))
                {
                    return gts;
                }
            }
            return null;
        }

        void ShowContent(int contentId)
        {
            SetContentView(contentId);

            switch (contentId)
            {
                case Resource.Layout.Main:
                    {                        
                        FindViewById<ImageButton>(Resource.Id.StartButton).Click += StartProgress;
                        FindViewById<ImageButton>(Resource.Id.theend).Click += (s, e) =>
                        {
                            Process.KillProcess(Process.MyPid());
                        };

                        break;
                    }
                case Resource.Layout.BpView:
                    {
                        timer.Stop();

                        var showVideo = FindViewById<VideoView>(Resource.Id.heartVideo);
                        showVideo.SetVideoPath($"android.resource://{AppContext.PackageName}/{Resource.Raw.heart}");
                        FindViewById<VideoView>(Resource.Id.heartVideo).Completion += (s, a) => showVideo.Start();

                        FindViewById<ProgressBar>(Resource.Id.progress).Max = 60;
                        FindViewById<ProgressBar>(Resource.Id.progress).Progress = 0;

                        FindViewById<VideoView>(Resource.Id.heartVideo).Start();
                        FindViewById<LinearLayout>(Resource.Id.showValue).Visibility = ViewStates.Invisible;
                        
                        timer.Start();

                        FindViewById<ImageButton>(Resource.Id.back).Click += (s, e) => ShowContent(Resource.Layout.Main);

                        FindViewById<ImageButton>(Resource.Id.reLoad).Click += StartProgress;

                        FindViewById<ImageButton>(Resource.Id.cloudupload).Click += (s, e) =>
                        {
                            if (!ApiClient.CheckConnection())
                            {
                                ShowContent(Resource.Layout.CloudLoginData);
                                return;
                            }

                            if (ApiClient.SendBP(lastSys, lastDia))
                            {
                                ShowContent(Resource.Layout.Main);
                                ShowToast("Send data success!");
                            }
                            else
                            {
                                ShowToast("Send data failed!");
                            }

                        };

                        break;
                    }
                case Resource.Layout.CloudLoginData:
                    {
                        FindViewById<EditText>(Resource.Id.loginemail).Text = ApiClient.Email;
                        FindViewById<EditText>(Resource.Id.loginpassword).Text = ApiClient.Password;

                        FindViewById<Button>(Resource.Id.loginbutton).Click += (s, e) =>
                        {

                            ApiClient.Email = FindViewById<EditText>(Resource.Id.loginemail).Text;
                            ApiClient.Password = FindViewById<EditText>(Resource.Id.loginpassword).Text;
                            if (!ApiClient.CheckConnection())
                            {
                                AlertDialog.Builder alert = new AlertDialog.Builder(this);
                                alert.SetTitle("Login error");
                                alert.SetMessage("Please check login data!");
                                alert.SetPositiveButton("Ok", (senderAlert, args) =>
                                {

                                });

                                Dialog dialog = alert.Create();
                                dialog.Show();
                            }
                            else
                            {
                                if (ApiClient.SendBP(lastSys, lastDia))
                                {
                                    ShowToast("Send data success!");
                                    ShowContent(Resource.Layout.Main);
                                }
                                else
                                {
                                    ShowToast("Send data failed!");
                                }
                            }

                        };

                        FindViewById<Button>(Resource.Id.cancellogin).Click += (s, e) =>
                        {
                            ShowContent(Resource.Layout.Main);
                        };

                        break;
                    }
            }
        }

        private bool _endProgress;
        public void ShowBP(byte sys, byte dia)
        {
            RunOnUiThread(() =>
            {
                Window.ClearFlags(WindowManagerFlags.KeepScreenOn);
                timer.Stop();
                if (_endProgress)
                {
                    return;
                }

                if (sys < 90)
                {
                    ShowContent(Resource.Layout.Main);
                    ShowToast("Failed measurement");
                    return;
                }

                lastSys = (byte)(sys + 15);
                lastDia = (byte)(dia + 8);

                FindViewById<VideoView>(Resource.Id.heartVideo).StopPlayback();
                FindViewById<VideoView>(Resource.Id.heartVideo).Visibility = ViewStates.Gone;
                FindViewById<ProgressBar>(Resource.Id.progress).Progress = 60;                
                FindViewById<TextView>(Resource.Id.bloodpreasure).Text = $"{lastSys}/{lastDia}";
                FindViewById<LinearLayout>(Resource.Id.showValue).Visibility = ViewStates.Visible;
                _endProgress = true;
            });
        }

        public void ShowToast(string body)
        {
            Toast.MakeText(this, body, ToastLength.Long).Show();
        }
    }

}

